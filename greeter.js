//import { TemplateService } from './fileReader';
var Student = (function () {
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
    return Student;
}());
var Greeter = (function () {
    function Greeter(user) {
        this.user = user;
    }
    Greeter.prototype.greet = function () {
        return "<h1> Hello, " + this.user.firstName + "</h1>";
    };
    return Greeter;
}());
;
var borderWidth = 4;
var draggin = false;
var topStart = 0;
var leftStart = 0;
var user = new Student("User", "M.", "User");
var content = new Greeter(user).greet();
var desktopSection = document.getElementById("desctopSection");
var programmJar = document.getElementById("divProgrammJar");
var toolBarItemTurnedWindow = document.getElementById("toolBarItemTurnedWindow");
var toolBarItemNewWindow = document.getElementById("toolBarItemNewWindow");
var draggableDiv = document.getElementById("draggableDiv");
var frameDiv = document.getElementById("frameDiv");
var closeButton = draggableDiv.getElementsByClassName("control-item-close")[0];
var turnButton = draggableDiv.getElementsByClassName("control-item-turn")[0];
var expandButton = draggableDiv.getElementsByClassName("control-item-expand")[0];
document.getElementById("windowContent").innerHTML = content;
draggableDiv.onmousedown = function () {
    var fullScrinWidth = window.innerWidth - borderWidth;
    var fullScrinHeight = window.innerHeight - borderWidth;
    draggableDiv.style.resize = 'both';
    draggableDiv.style.maxWidth = fullScrinWidth - draggableDiv.clientLeft + 'px';
    draggableDiv.style.maxHeight = fullScrinHeight - draggableDiv.clientTop + 'px';
};
frameDiv.onmousedown = function (event) {
    var fullScrinWidth = window.innerWidth - borderWidth;
    if (draggableDiv.style.width === fullScrinWidth + 'px')
        return;
    topStart = event.clientY - draggableDiv.offsetTop;
    leftStart = event.clientX - draggableDiv.offsetLeft;
    draggin = true;
};
draggableDiv.onmouseup = function () {
    draggin = false;
};
document.body.onmousemove = function (event) {
    var fullScrinWidth = window.innerWidth - borderWidth;
    var fullScrinHeight = window.innerHeight - borderWidth;
    if (event.clientY > (fullScrinHeight - 5) || event.clientX > (fullScrinWidth - 5)) {
        draggin = false;
        //draggableDiv.style.resize = 'none';
        return;
    }
    if (!draggin)
        return;
    var newTop = event.clientY - topStart;
    var newLeft = event.clientX - leftStart;
    if (newTop < 0 || (newTop + draggableDiv.clientHeight) > fullScrinHeight)
        return;
    if (newLeft < 0 || (newLeft + draggableDiv.clientWidth) > fullScrinWidth)
        return;
    draggableDiv.style.top = newTop + 'px';
    draggableDiv.style.left = newLeft + 'px';
};
expandButton.onclick = function () {
    var fullScrinWidth = window.innerWidth - borderWidth;
    var fullScrinHeight = window.innerHeight - borderWidth;
    draggableDiv.style.maxWidth = fullScrinWidth + 'px';
    draggableDiv.style.maxHeight = fullScrinHeight + 'px';
    if (draggableDiv.style.width !== fullScrinWidth + 'px') {
        draggableDiv.style.top = '0px';
        draggableDiv.style.left = '0px';
        draggableDiv.style.width = fullScrinWidth + 'px';
        draggableDiv.style.height = fullScrinHeight + 'px';
    }
    else {
        draggableDiv.style.width = '400px';
        draggableDiv.style.height = '300px';
    }
};
closeButton.onclick = function () {
    draggableDiv.style.visibility = 'hidden';
    toolBarItemTurnedWindow.style.visibility = 'hidden';
    //draggableDiv.remove();
};
turnButton.onclick = function () {
    draggableDiv.style.visibility = 'hidden';
    //toolBarItemTurnedWindow.style.visibility = 'visible';
};
toolBarItemTurnedWindow.onclick = function () {
    draggableDiv.style.visibility = 'visible';
};
programmJar.onclick = function () {
    if (!draggableDiv)
        return;
    if (draggableDiv.style.visibility !== 'visible') {
        draggableDiv.style.width = '400px';
        draggableDiv.style.height = '300px';
        draggableDiv.style.visibility = 'visible';
        toolBarItemTurnedWindow.style.visibility = 'visible';
    }
};
Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
};
